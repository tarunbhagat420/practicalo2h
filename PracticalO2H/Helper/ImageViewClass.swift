//
//  ImageViewClass.swift
//  PracticalO2H
//
//  Created by Tarun Bhagat on 24/07/23.
//

import Foundation
import UIKit

class ImageViewClass : UIImageView {
    let imageCache: NSCache<NSString, UIImage> = NSCache()
    var imageUrlString : String?
    
    func loadImageUsingUrl(urlString: String) {
        imageUrlString = urlString
        
        if let url = URL(string: urlString) {
            if let imageFromCache = imageCache.object(forKey: urlString as NSString){
                self.image = imageFromCache
                return
            }else{
                image = nil
            }
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    print(error as Any)
                }
                if data != nil {
                    DispatchQueue.main.async {
                        if let imageToCache = UIImage(data: data!) {
                            if self.imageUrlString == urlString {
                                self.image = imageToCache
                            }
                            self.imageCache.setObject(imageToCache, forKey: urlString  as NSString)
                        }
                        
                    }
                }
                    
            }.resume()
        }else{
            self.image = UIImage(named: "avatar")
        }
    }
}
