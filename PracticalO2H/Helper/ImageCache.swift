//
//  ImageCatch.swift
//  PracticalO2H
//
//  Created by Tarun Bhagat on 25/07/23.
//

import UIKit

class ImageCache {
    static let shared = ImageCache()
    private var cache = NSCache<NSString, UIImage>()
    
    private init() {}
    
    func saveImageToCache(image: UIImage, key: String) {
        cache.setObject(image, forKey: key as NSString)
    }
    
    func loadImageFromCache(key: String) -> UIImage? {
        return cache.object(forKey: key as NSString)
    }
}
