//
//  ApiManager.swift
//  PracticalO2H
//
//  Created by Tarun Bhagat on 24/07/23.
//

import Foundation

import UIKit

typealias Handler = ( Result<[DataModel], DataError> ) -> Void

final class ApiManager {
    
    static let shared = ApiManager()
    private init() {}
    
    func fetchProduct(complition: @escaping Handler) {
        guard let url = URL(string: Constant.ConstantApi.productUrl) else { return }
        
        URLSession.shared.dataTask(with: url) { data, response, erroe in
            
            guard let data = data, erroe == nil else {
                complition(.failure(.invalidData))
                return
            }
            
            guard let response = response as? HTTPURLResponse,
                  200 ... 299 ~= response.statusCode else {
                complition(.failure(.invalidResponse))
                return
            }
            
            do {
                let products = try JSONDecoder().decode([DataModel].self, from: data)
                complition(.success(products))
            } catch {
                complition(.failure(.network(error)))
                
            }
            
        }.resume()
    }
    
}
