//
//  DataError.swift
//  PracticalO2H
//
//  Created by Tarun Bhagat on 24/07/23.
//

import Foundation

enum DataError: Error {
    case invalidResponse
    case invalidURL
    case invalidData
    case network(Error?)
}
