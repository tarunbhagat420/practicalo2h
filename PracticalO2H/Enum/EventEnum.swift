//
//  EventEnum.swift
//  PracticalO2H
//
//  Created by Tarun Bhagat on 24/07/23.
//

import Foundation

enum Event {
    case loading
    case stopLoading
    case dataLoaded
    case error(Error?)
}
