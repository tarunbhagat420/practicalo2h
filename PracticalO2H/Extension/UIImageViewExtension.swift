//
//  UIImageViewExtension.swift
//  PracticalO2H
//
//  Created by Tarun Bhagat on 25/07/23.
//

import UIKit
import Kingfisher

extension UIImageView {
    
    func setImage(<#parameters#>) -> <#return type#> {
        <#function body#>
    }
    
    func setImage(urlString: String) {
        guard let url = URL.init(string: urlString) else {
            return
        }
        
        let resource = ImageResource(downloadURL: url, cacheKey: urlString)
        kf.indicatorType = .activity
        kf.setImage(with: resource)
    }
}
