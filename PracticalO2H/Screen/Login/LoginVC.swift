//
//  LoginVc.swift
//  PracticalO2H
//
//  Created by Tarun Bhagat on 24/07/23.
//

import Foundation
import UIKit
import GoogleSignIn

class LoginVC: UIViewController {
    
    let mainStory = UIStoryboard.init(name: "Main", bundle: nil)
    let loginstory = UIStoryboard.init(name: "Login", bundle: nil)
        
    override func viewDidLoad() {
        super.viewDidLoad()
                
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        GIDSignIn.sharedInstance()?.presentingViewController = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false

    }
    
    func popUpAlert(title: String, message: String,actiontitle: [String],actionstyle: [UIAlertAction.Style],action: [(UIAlertAction) -> Void ] ) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for(index,title) in actiontitle.enumerated() {
            let action = UIAlertAction(title: title, style: actionstyle[index], handler: action[index])
            
            alert.addAction(action)
        }
        present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func logInClicked(_ sender: Any) {
        
        GIDSignIn.sharedInstance().delegate=self
        GIDSignIn.sharedInstance().clientID = "226377657241-7mo9gio5m3d7lk7ioodnt562046ukqvp.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().signIn()
    }
    
}

extension LoginVC: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
                // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            var profilepic : String = ""
                // ...
            print(userId as Any)
            print(idToken as Any)
            print(fullName as Any)
            print(givenName as Any)
            print(familyName as Any)
            print(email as Any)
                
            if user.profile.hasImage
            {
                let pic = user.profile.imageURL(withDimension: 100)
                profilepic = pic?.absoluteString ?? ""
            }
            print(profilepic)

                
            if (email != nil)  && (fullName != nil){
                self.view.isUserInteractionEnabled = false
                if let uid = userId {
                    DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                        print("userid \(uid)")
                        
                        DispatchQueue.main.async {
                            UserDefaults.standard.set("1", forKey: "loginstatus")
                            UserDefaults.standard.setValue(fullName, forKey: "userFullName")
                            
                            let vc = self.mainStory.instantiateViewController(withIdentifier: "ImageListVC") as! ImageListVC
                            vc.userEmail = email ?? "No Email"
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }else{
                    self.popUpAlert(title: "Something went wrong", message: "Please try again later", actiontitle: ["Ok"], actionstyle: [.default], action: [
                        { ok in
                            self.view.isUserInteractionEnabled = true
                            print("OK")
                        }
                    ])

                }

            }
        }
    }
}
