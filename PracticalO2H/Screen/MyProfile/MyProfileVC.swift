//
//  MyProfileVC.swift
//  PracticalO2H
//
//  Created by Tarun Bhagat on 24/07/23.
//

import Foundation
import UIKit

class MyProfileVC: UIViewController {
    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    let mainStory = UIStoryboard.init(name: "Main", bundle: nil)
    let loginstory = UIStoryboard.init(name: "Login", bundle: nil)
    
    
    var userEmail: String = ""
        
    override func viewDidLoad() {
        super.viewDidLoad()
        lblEmail.text = UserDefaults.standard.string(forKey: "userFullName")
                    
    }
    
    @IBAction func btnLogoutClicked(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "loginstatus")
        UserDefaults.standard.removeObject(forKey: "userFullName")
        
        let vc = self.loginstory.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
