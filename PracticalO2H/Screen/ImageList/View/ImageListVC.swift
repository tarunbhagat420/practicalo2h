//
//  ViewController.swift
//  PracticalO2H
//
//  Created by Tarun Bhagat on 24/07/23.
//

import UIKit
import Kingfisher

class ImageListVC: UIViewController {
    
    @IBOutlet weak var btnMyProfile: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var viewModel = ImageListViewModel()
    
    let mainStory = UIStoryboard.init(name: "Main", bundle: nil)
    let loginstory = UIStoryboard.init(name: "Login", bundle: nil)
    
    var userEmail: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configuration()
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.minimumLineSpacing = 0
        collectionView.collectionViewLayout = flowLayout
        collectionView.dataSource = self
        collectionView.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false

    }

    @IBAction func btnMyProfileTapped(_ sender: Any) {
        
        let vc = self.mainStory.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
        
        vc.userEmail = self.userEmail
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
}

extension ImageListVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return viewModel.data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageCell

        let data = viewModel.data[indexPath.row]
        
        // Create a URL for the local file
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let imageName = "\(data.id).jpg"
            let imageURL = documentsDirectory.appendingPathComponent(imageName)
                
            // Check if the image is available in the cache
            if let cachedImage = ImageCache.shared.loadImageFromCache(key: data.id) {
                cell.cellImage.image = cachedImage
            } else if let localImage = UIImage(contentsOfFile: imageURL.path) {
                // Load the image from the local file system
                cell.cellImage.image = localImage
                ImageCache.shared.saveImageToCache(image: localImage, key: data.id)
            } else {
                // Set a placeholder image or show a loading indicator while fetching the image
                cell.cellImage.image = UIImage(named: "placeholder")
                    
                // Fetch the image from the server (if needed) and update the cell once it's available
                DispatchQueue.global().async {
                    if let imageUrl = URL(string: data.download_url),
                        let imageData = try? Data(contentsOf: imageUrl),
                        let image = UIImage(data: imageData) {
                        DispatchQueue.main.async {
                            cell.cellImage.image = image
                            ImageCache.shared.saveImageToCache(image: image, key: data.id)
                            // Save the image to the local file system for future use
                            try? imageData.write(to: imageURL)
                        }
                    }
                }
            }
        }
        
        
        
//        let imageURL = URL(string: data.download_url)
//
//        cell.cellImage.kf.setImage(with: imageURL)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.bounds.width
        let width = collectionViewWidth / 3.0

        return CGSize(width: width, height: width + 20)
    }
}

extension ImageListVC {
    func configuration() {
        initViewModel()
        observeEvent()
    }
    func initViewModel() {
        viewModel.fetchData()
    }
    func observeEvent() {
        viewModel.eventHandler = { [weak self] event in
            guard let self = self else { return }
            
            switch event {
            case .loading:
                print("Data Loading...")
            case .stopLoading:
                print("Stop Loading...")
            case .dataLoaded:
                print("Data Loaded")
                print(self.viewModel.data)
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            case .error(let error):
                print(error)
            }
        }
    }
}

