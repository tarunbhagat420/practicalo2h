//
//  ImageCell.swift
//  PracticalO2H
//
//  Created by Tarun Bhagat on 24/07/23.
//

import UIKit

class ImageCell: UICollectionViewCell {
        
    @IBOutlet weak var cellImage: UIImageView!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
