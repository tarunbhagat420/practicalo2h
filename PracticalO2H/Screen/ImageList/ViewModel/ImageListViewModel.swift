//
//  ImageListViewModel.swift
//  PracticalO2H
//
//  Created by Tarun Bhagat on 24/07/23.
//

import Foundation
import UIKit

final class ImageListViewModel {
    
    var data: [DataModel] = []
    var eventHandler: ((_ event: Event) -> Void)?
    
    private let imageCache = NSCache<NSString, UIImage>()
    
    func fetchData() {
        self.eventHandler?(.loading)
        
        // Check for internet connectivity
        if !NetworkReachability.isConnectedToNetwork() {
            // No internet connection, try loading data from the local cache
            self.loadOfflineData()
            return
        }
        
        ApiManager.shared.fetchProduct { response in
            self.eventHandler?(.stopLoading)
            switch response {
            case .success(let data):
                self.data = data
                
                // Load images and cache them
                DispatchQueue.global().async {
                    let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                    for index in data.indices {
                        let imageUrlString = data[index].download_url
                        if let imageUrl = URL(string: imageUrlString),
                            let imageData = try? Data(contentsOf: imageUrl),
                            let image = UIImage(data: imageData) {
                            // Cache the image
                            ImageCache.shared.saveImageToCache(image: image, key: data[index].id)
                                        
                            // Save the image to the local file system (Optional: only saves if not already saved)
                            if let documentsDirectory = documentsDirectory {
                                let imageName = "\(data[index].id).jpg"
                                let imageURL = documentsDirectory.appendingPathComponent(imageName)
                                if !FileManager.default.fileExists(atPath: imageURL.path) {
                                    try? imageData.write(to: imageURL)
                                }
                            }
                        }
                    }
                                
                    // Notify the main queue to update the collection view
                    DispatchQueue.main.async {
                        self.eventHandler?(.dataLoaded)
                    }
                }
                
                
                self.eventHandler?(.dataLoaded)
            case .failure(let error):
                self.loadOfflineData()
                self.eventHandler?(.error(error))
            }
        }
    }
    
    func loadOfflineDataModels() -> [DataModel] {
        var dataModels: [DataModel] = []
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURL = documentsDirectory.appendingPathComponent("dataModels.json")
            do {
                let jsonData = try Data(contentsOf: fileURL)
                let decoder = JSONDecoder()
                dataModels = try decoder.decode([DataModel].self, from: jsonData)
            } catch {
                print("Error loading data: \(error)")
            }
        }
        return dataModels
    }
    
    func loadOfflineData() {
        // Load data from the local cache here and update the collection view
        self.data = loadOfflineDataModels()
        self.eventHandler?(.dataLoaded)
    }
}
